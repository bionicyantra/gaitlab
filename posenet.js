let video;
let poseNet;
let poses = [];
let tblHeaders = ["Part", "x-position", "y-position", "score"];
//Display max 50 rows at a time, change this if more rows are needed
let rowMax = 50; 
//Get max 10000 rows at a time, change this if more rows are needed
let xlMax = 10000;
// default probablity, change this to have higher probability if required
let scoreProbability = 0.2
let table = document.querySelector("table");
let tbody = table.tBodies[0]
let gaitData = [];


function setup() {
    const canvas = createCanvas(640, 480);
    canvas.parent('videoContainer');

    // Video capture
    video = createCapture(VIDEO);
    video.size(width, height);

    // Create a new poseNet method with a single detection
    poseNet = ml5.poseNet(video, modelReady);

    poseNet.on('pose', function (results) {
        poses = results;
    });

    // Hide the video element, and just show the canvas
    video.hide();
}

function draw() {
    image(video, 0, 0, width, height);

    // We can call both functions to draw all keypoints and the skeletons
    drawKeypoints();
    drawSkeleton();
    drawPosition();
}

function modelReady() {
    select('#status').html('model Loaded')
}


// A function to draw ellipses over the detected keypoints
function drawKeypoints() {
    // Loop through all the poses detected
    for (let i = 0; i < poses.length; i++) {
        // For each pose detected, loop through all the keypoints
        let pose = poses[i].pose;
        for (let j = 0; j < pose.keypoints.length; j++) {
            // A keypoint is an object describing a body part (like rightArm or leftShoulder)
            let keypoint = pose.keypoints[j];
            // Only draw an ellipse is the pose probability is bigger than 0.2
            if (keypoint.score > scoreProbability) {
                fill(255, 0, 0);
                noStroke();
                ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
            }
        }
    }
}

// A function to draw the skeletons
function drawSkeleton() {
    // Loop through all the skeletons detected
    for (let i = 0; i < poses.length; i++) {
        let skeleton = poses[i].skeleton;
        // For every skeleton, loop through all body connections
        for (let j = 0; j < skeleton.length; j++) {
            let partA = skeleton[j][0];
            let partB = skeleton[j][1];
            stroke(255, 0, 0);
            line(partA.position.x, partA.position.y, partB.position.x, partB.position.y);
        }
    }
}

function drawPosition() {
	
	let data = Object.keys(tblHeaders);
    var ctx = canvas.getContext("2d")
    for (let i = 0; i < poses.length; i++) {
        // For each pose detected, loop through all the keypoints
        let pose = poses[i].pose;
        for (let j = 0; j < pose.keypoints.length; j++) {
			let partObj = new Object();
            // A keypoint is an object describing a body part (like rightArm or leftShoulder)
            let keypoint = pose.keypoints[j];
            // Only draw an ellipse is the pose probability is bigger than 0.2
            if (keypoint.score > scoreProbability) {
                word = keypoint.part + "," + keypoint.position.x + "," + keypoint.position.y
                //ctx.fillText(word, i + 50, j + 50)
                //console.log(keypoint)
				partObj["Part"] = keypoint.part;
				partObj["x-position"] = keypoint.position.x.toFixed(3);
				partObj["y-position"] = keypoint.position.y.toFixed(3);
				partObj["score"] = keypoint.score.toFixed(2);
				if (gaitData.length < xlMax) {
					gaitData.unshift(partObj);
				}
				var rowCount = table.rows.length;
				if (rowCount > rowMax){
					table.deleteRow(-1);
				}
				tr = tbody.insertRow(0);
				for (key in partObj) {
					var td = document.createElement('td');
					td.innerHTML = partObj[key];
					tr.appendChild(td);
				}
				
			}
        }
    }
}


function getXLSheet() {
	console.log("Exporting.....")
	$("#excelButton").excelexportjs({
	  containerid: "excelButton", 
	  datatype: 'json', 
	  dataset: gaitData, 
	  columns: getColumns(gaitData)     
	});

	console.log("XL Export Completed")
	
}