# ByGait

ByGait is a initiative to detect human points using the pose net library.

## Prerequisite

Please install python3.


## Usage

1. Start the server and give a port.
```python
python -m http.server 8010
```

2. Open your browser and 127.0.0.1:8010

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## License
Open Source